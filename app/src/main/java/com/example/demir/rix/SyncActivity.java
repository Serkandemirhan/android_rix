package com.example.demir.rix;
//import com.google.gson.Gson;
import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import android.provider.Settings.Secure;

import javax.net.ssl.HttpsURLConnection;


public class SyncActivity extends AppCompatActivity {
    Button btnsync,btngetir;
    private ProgressDialog dialog;
    String deger;


    DBHandler db;

    private ListView lvMovies;
    EditText etResponse,etServer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final JSONObject yt;


        dialog = new ProgressDialog(this);
        dialog.setIndeterminate(true);
        dialog.setCancelable(false);
        dialog.setMessage("Yükleniyor. Lütfen Bekleyin..."); // showing a dialog for loading the data


        btnsync = (Button) findViewById(R.id.btnSync);
        btngetir = (Button) findViewById(R.id.btnGetir);
        etResponse = (EditText) findViewById(R.id.etREsponse);
        etServer = (EditText) findViewById(R.id.etServer);

        btngetir.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String android_id = Secure.getString(getApplicationContext().getContentResolver(),  Secure.ANDROID_ID);
                String imei;
                imei = android_id;

                db = new DBHandler(getApplicationContext()); // Db bağlantısı oluşturuyoruz. İlk seferde database oluşturulur.
                deger = db.getResults().toString();//kitap listesini alıyoruz
                deger = "client_id=" + imei + "&tableversiyon={\"tableversiyon\":" + deger + "}";
                //deger=db.kitapDetay(2).toString();
                etResponse.setText(deger);



            }
            });



        btnsync.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

//                JSONTaskServer P =null;
//                try {
//                   P.uploadToServer("http://sync.kazel.net/1/api/sync/", deger);
//
//                } catch (IOException e) {
//
//                    return;
//
//                } catch (JSONException et) {
//
//                }
//
//                etServer.setText(P.toString());

            }

        });

    }

//
//    public class JSONTaskServer extends AsyncTask<String,String,String> {
//
//
//        JSONObject sonuc;
//
//
//
//        public JSONObject uploadToServer(String urll, String jsonobject) throws IOException, JSONException {
//            String query = urll;
//            String json = jsonobject;
//
//
//            URL url = new URL(query);
//            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//            conn.setConnectTimeout(5000);
//            conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
//            conn.setDoOutput(true);
//            conn.setDoInput(true);
//            conn.setRequestMethod("POST");
//
//            OutputStream os = conn.getOutputStream();
//            os.write(json.getBytes("UTF-8"));
//            os.close();
//
//            // read the response
//            InputStream in = new BufferedInputStream(conn.getInputStream());
//            String result = org.apache.commons.io.IOUtils.toString(in, "UTF-8");
//            JSONObject jsonObject = new JSONObject(result);
//
//
//            in.close();
//            conn.disconnect();
//
//            return jsonObject;
//        }
//
//
//        @Override
//        protected String doInBackground(String... params) throws IOException, JSONException {
//
//
//            String query = params[0];
//            String json = params[1];
//
//
//            URL url = new URL(query);
//            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//            conn.setConnectTimeout(5000);
//            conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
//            conn.setDoOutput(true);
//            conn.setDoInput(true);
//            conn.setRequestMethod("POST");
//
//            OutputStream os = conn.getOutputStream();
//            os.write(json.getBytes("UTF-8"));
//            os.close();
//
//            // read the response
//            InputStream in = new BufferedInputStream(conn.getInputStream());
//            String result = org.apache.commons.io.IOUtils.toString(in, "UTF-8");
//            JSONObject jsonObject = new JSONObject(result);
//
//            sonuc=jsonObject;
//
//
//            in.close();
//            conn.disconnect();
//
//            return "true";
//
//
//
//
//        }
//    }
//
//
//
//
//    public class JSONTask extends AsyncTask<String,String, String > {
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            dialog.show();
//        }
//        @Override
//        protected String doInBackground(String... params) {
//            HttpURLConnection connection = null;
//            BufferedReader reader = null;
//            try {
//                URL url = new URL(params[0]);
//                connection = (HttpURLConnection) url.openConnection();
//                connection.connect();
//                InputStream stream = connection.getInputStream();
//                reader = new BufferedReader(new InputStreamReader(stream));
//                StringBuffer buffer = new StringBuffer();
//                String line ="";
//                while ((line = reader.readLine()) != null){
//                    buffer.append(line);
//                }
//
//                String finalJson = buffer.toString();
//
//                //JSONObject parentObject = new JSONObject(finalJson);
//                //JSONArray parentArray = parentObject.getJSONArray("movies");
//                return finalJson;
//               // return parentObject.toString();
//
//            } catch (MalformedURLException e) {
//                e.printStackTrace();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            //catch (JSONException e) {  e.printStackTrace();  }
//
//
//            finally {
//                if(connection != null) {
//                    connection.disconnect();
//                }
//                try {
//                    if(reader != null) {
//                        reader.close();
//                    }
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//            return  null;
//        }
//
//
//
//        @Override
//        protected void onPostExecute(String result) {
//            super.onPostExecute(result);
//            dialog.dismiss();
//            etServer.setText(result);
//
//        }
//    }
//


}
