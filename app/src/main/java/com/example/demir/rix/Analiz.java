package com.example.demir.rix;

public class Analiz {
    private int analiz_id;
    private int firma_id;
    private int sube_id;
    private String firma;
    private String sube;
    private String tarih;
    private String danisman;

    public Analiz(int analiz_id, int firma_id, int sube_id, String firma, String sube, String tarih, String danisman) {
        this.analiz_id = analiz_id;
        this.firma_id=firma_id;
        this.sube_id=sube_id;
        this.firma = firma;
        this.sube = sube;
        this.tarih=tarih;
        this.danisman=danisman;
    }
    public int getId() {
        return analiz_id;
    }

    public String getFirma() {
        return firma;
    }
    public String getSube() {
        return sube;
    }

    public String getTarih() {
        return tarih;
    }
    public String getDanisman() {
        return danisman;
    }
    public String Firma() {
        return firma;
    }

}