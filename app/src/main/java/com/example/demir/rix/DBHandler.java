package com.example.demir.rix;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Console;
import java.util.ArrayList;
import java.util.HashMap;

import static android.app.PendingIntent.getActivity;

/**
 * Created by demir on 04.10.2017.
 */

public class DBHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 2;
    private static final String DATABASE_NAME = "rix_client_101";
    private static final String CLIENT_NAME="rix_client_1";



    public DBHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {


        String analiz;
        analiz = "CREATE TABLE IF NOT EXISTS analiz(" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "server_id INTEGER," +
                "versiyon INTEGER," +
                "sync INTEGER DEFAULT 0," +
                "aktif INTEGER DEFAULT 1," +
                "soru TEXT," +
                "cevap TEXT)";

        String firma;
        firma = "CREATE TABLE IF NOT EXISTS firma (" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "server_id INTEGER," +
                "versiyon INTEGER," +
                "sync INTEGER DEFAULT 0," +
                "aktif INTEGER DEFAULT 1," +
                "adi TEXT," +
                "email TEXT)";

        String tableversiyon;
        tableversiyon = "CREATE TABLE IF NOT EXISTS tableversiyon (" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "tablo TEXT," +
                "versiyon INTEGER)";



        String inserttableversiyon =  "INSERT INTO tableversiyon (tablo,versiyon) VALUES" + "('analiz',1),"+
                 "('firmalar',1),"+
                 "('diger3',1),"+
                 "('diger4',1),"+
                 "('diger5',1);"    ;

//        String inserttableversiyon2 =  "INSERT INTO tableversiyon (tablo,versiyon) VALUES" + "('diger',1)" ;
//        String inserttableversiyon3 = "INSERT INTO tableversiyon (tablo,versiyon) VALUES" + "('xyz',1)" ;
//        String inserttableversiyon4 = "INSERT INTO tableversiyon (tablo,versiyon) VALUES"  +"('firma',1)";
//        String inserttableversiyon5 = "INSERT INTO tableversiyon (tablo,versiyon) VALUES"  +"('defe',1)";

//
        try {
            db.execSQL(analiz);
            db.execSQL(firma);
            db.execSQL(tableversiyon);
            db.execSQL(inserttableversiyon);
//            db.execSQL(inserttableversiyon2);
//            db.execSQL(inserttableversiyon3);
//            db.execSQL(inserttableversiyon4);
//            db.execSQL(inserttableversiyon5);



            // Log.d("serkan", analiz);
//        adresilEkle("Ankara");
//        adresilEkle("Yozgat");
            Log.e("Created", "Created Both the Tables");

        }


        catch (Error e) {

            String errorr=e.toString();
            Log.e("Created", errorr);

        }

    }

    public void addTablo(SQLiteDatabase db, String tablo, String versiyon) {
        ContentValues values = new ContentValues();
        values.put(tablo, tablo);
        values.put(versiyon, versiyon);
        db.insert("tableversiyon", null, values);
    }

        // *****  Retrieve all table versiyon  *******


    public JSONArray getResults() {
        SQLiteDatabase db = this.getReadableDatabase();


        String searchQuery = "SELECT  * FROM tableversiyon";
        Cursor cursor = db.rawQuery(searchQuery, null);
        Log.d("total Rows", Integer.toString(cursor.getCount()));

        JSONArray resultSet = new JSONArray();
        JSONObject returnObj = new JSONObject();

        cursor.moveToFirst();
        while (cursor.isAfterLast() == false) {

            int totalColumn = cursor.getColumnCount();

            JSONObject rowObject = new JSONObject();

            for (int i = 0; i < totalColumn; i++) {
                if (cursor.getColumnName(i) != null) {

                    try {

                        if (cursor.getString(i) != null) {
                            Log.d("TAG_NAME", cursor.getString(i));
                            rowObject.put(cursor.getColumnName(i), cursor.getString(i));
                        } else {
                            rowObject.put(cursor.getColumnName(i), "");
                        }
                    } catch (Exception e) {
                        Log.d("TAG_NAME", e.getMessage());
                    }

                }

            }
            resultSet.put(rowObject);
            cursor.moveToNext();
        }

        cursor.close();
        Log.d("TAG_NAME", resultSet.toString());
        return resultSet;
    }


    public void adresilSil(int id){ //id si belli olan row u silmek için

        SQLiteDatabase db = this.getWritableDatabase();
        db.delete("adresil",  "id = ?",
                new String[] { String.valueOf(id) });
        db.close();
    }
    public void tabloEkle(String adi ) {
        //kitapEkle methodu ise adı üstünde Databese veri eklemek için
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("tablo", adi);
        db.insert("tabloversiyon",null, values);
        db.close(); //Database Bağlantısını kapattık*/
    }

    public String kitapDetay(int id){
        //Databeseden id si belli olan row u çekmek için.
        //Bu methodda sadece tek row değerleri alınır.
        //HashMap bir çift boyutlu arraydir.anahtar-değer ikililerini bir arada tutmak için tasarlanmıştır.
        //map.put("x","300"); mesala burda anahtar x değeri 300.

        HashMap<String,String> tablolist = new HashMap<String,String>();
        String selectQuery = "SELECT * FROM tableversiyon";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // Move to first row

        int rowCount = cursor.getCount();

        Log.e("degerler toplam kayit", Integer.toString(rowCount));
        cursor.moveToFirst();
        if(cursor.getCount() > 0){
            tablolist.put("id", cursor.getString(0));
            tablolist.put("tablo", cursor.getString(1));
            tablolist.put("versiyon", cursor.getString(2));
            Log.e("degerler",cursor.getString(0)+cursor.getString(1)+cursor.getString(2));
        }
        cursor.close();
        db.close();
        // return kitap
        return tablolist.toString();
    }

    public ArrayList<HashMap<String, String>> kitaplar(){

        //Bu methodda ise tablodaki tüm değerleri alıyoruz
        //ArrayList adı üstünde Array lerin listelendiği bir Array.Burda hashmapleri listeleyeceğiz
        //Herbir satırı değer ve value ile hashmap a atıyoruz. Her bir satır 1 tane hashmap arrayı demek.
        //olusturdugumuz tüm hashmapleri ArrayList e atıp geri dönüyoruz(return).

        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM adresil";
        Cursor cursor = db.rawQuery(selectQuery, null);
        ArrayList<HashMap<String, String>> adresillist = new ArrayList<HashMap<String, String>>();
        // looping through all rows and adding to list

        if (cursor.moveToFirst()) {
            do {
                HashMap<String, String> map = new HashMap<String, String>();
                for(int i=0; i<cursor.getColumnCount();i++)
                {
                    map.put(cursor.getColumnName(i), cursor.getString(i));
                }

                adresillist.add(map);
            } while (cursor.moveToNext());
        }
        db.close();
        // return kitap liste
        return adresillist;
    }

    public void adresilDuzenle(String kitap_adi, String kitap_yazari,String kitap_basim_yili,String kitap_fiyat,int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        //Bu methodda ise var olan veriyi güncelliyoruz(update)
        ContentValues values = new ContentValues();
        values.put("adi", kitap_adi);
        values.put("versiyon", kitap_yazari);
        values.put("sync", kitap_basim_yili);
        values.put("aktif", kitap_fiyat);

        // updating row
        db.update("adresil", values,"id"+ " = ?",
                new String[] { String.valueOf(id) });
    }

    public int getRowCount() {
        // Bu method bu uygulamada kullanılmıyor ama her zaman lazım olabilir.Tablodaki row sayısını geri döner.
        //Login uygulamasında kullanacağız
        String countQuery = "SELECT  * FROM " + "tableversiyon";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int rowCount = cursor.getCount();
        db.close();
        cursor.close();
        // return row count
        return rowCount;
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }


    public ArrayAdapter<String> tumKayitlar(Context context)
    {
        String[] sutunlar = new String[] { "id","adi", "versiyon", "sync","aktif"};
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.query("firma", sutunlar, null, null,null, null, null);



        int idNo=c.getColumnIndex("id");

        int adi = c.getColumnIndex("adi");
        int versiyon = c.getColumnIndex("versiyon");
        int sync = c.getColumnIndex("sync");
        int aktif = c.getColumnIndex("aktif");

        String dizi[]=new String[c.getCount()];
        int sayac=0;
        for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
            dizi[sayac]=c.getString(idNo) + "    "  + c.getString(adi) + "    "  + c.getString(versiyon) + "    "  + c.getString(sync) + "  " + c.getString(aktif);
            sayac+=1;
        }

        ArrayAdapter AA= new ArrayAdapter<String>(context,android.R.layout.simple_list_item_1,dizi);
        return AA;
    }
}
