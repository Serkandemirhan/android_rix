package com.example.demir.rix;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.SimpleMultiPartRequest;
import com.android.volley.toolbox.Volley;

/**
 * Created by demir on 20.10.2017.
 */

public class InternetCheck implements Response.ErrorListener {

    public boolean internet  =false;

    public static class WebService {
        private RequestQueue mRequestQueue;
        private static WebService apiRequests = null;

        public static WebService getInstance() {
            if (apiRequests == null) {
                apiRequests = new WebService();
                return apiRequests;
            }
            return apiRequests;
        }
        public void tryLogin(Context context,
                             String username,
                             String password,
                             String subdomain,
                             Response.Listener<String> listener,
                             Response.ErrorListener errorListener) {
            SimpleMultiPartRequest request = new SimpleMultiPartRequest(Request.Method.POST, "http://ws.kazel.net/master/login", listener, errorListener);
//        request.setParams(data);
            mRequestQueue = RequestManager.getnstance(context);
            //request.addMultipartParam("token", "text", "tdfysghfhsdfh");
            request.addMultipartParam("username", "a", username);
            request.addMultipartParam("password", "a", password);
            request.addMultipartParam("subdomain", "serkan", subdomain);
            //request.addFile("document_file", file.getPath());

            request.setFixedStreamingMode(true);
            mRequestQueue.add(request);
        }
    }
    public static class DataWebService {
        private RequestQueue mRequestQueue;
        private static DataWebService apiRequests = null;

        public static DataWebService getInstance() {
            if (apiRequests == null) {
                apiRequests = new DataWebService();
                return apiRequests;
            }
            return apiRequests;
        }
        public void tryGetir(Context context,
                             String token,
                             String subdomain,
                             String servisname,
                             Response.Listener<String> listener,
                             Response.ErrorListener errorListener) {
            SimpleMultiPartRequest request = new SimpleMultiPartRequest(Request.Method.POST, "http://ws.kazel.net/firma/"+servisname, listener, errorListener);
//        request.setParams(data);
            mRequestQueue = RequestManager.getnstance(context);
            //request.addMultipartParam("token", "text", "tdfysghfhsdfh");
            request.addMultipartParam("token", "text", token);
            request.addMultipartParam("subdomain", "text", subdomain);
            //request.addFile("document_file", file.getPath());
            request.setFixedStreamingMode(true);
            mRequestQueue.add(request);
        }
    }

    static class RequestManager {
        private static RequestManager mRequestManager;
        /**
         * Queue which Manages the Network Requests :-)
         */
        private static RequestQueue mRequestQueue;
        // ImageLoader Instance

        private RequestManager() {

        }

        public static RequestManager get(Context context) {

            if (mRequestManager == null)
                mRequestManager = new RequestManager();

            return mRequestManager;
        }

        /**
         * @param context application context
         */
        public static RequestQueue getnstance(Context context) {

            if (mRequestQueue == null) {
                mRequestQueue = Volley.newRequestQueue(context);
            }

            return mRequestQueue;

        }

    }


    public void xnternetCheck(Context context) {


        Response.Listener<String> deneme =new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

               // Log.d("xxResponse",response);
                try {
                    Integer code=0;
                    code = Integer.parseInt( response.substring(38, 42));
                    if(code==1024){
                        internet=true;
                    }
                }
                catch (Exception e){
                    internet=false;
                   // System.out.print(e);
                }


            }
        };

        Response.ErrorListener x;
        WebService.getInstance().tryLogin(

                context
                //getActivity().getApplicationContext()
                , "a", "b", "serkan",deneme, this);

    }


    @Override
    public void onErrorResponse(VolleyError error) {

        Log.d("Hata",error.getMessage());

    }
}
