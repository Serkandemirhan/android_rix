package com.example.demir.rix;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.vkondrav.swiftadapter.SwiftAdapter;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

	static String Firma;
    int resultcode;
	static String Sube;
	Integer CompanyID;
    Integer SubeID;
	Adapter adapter;
    static String dDepartman;
    static String dFaaliyet;
    static String dSoru;
    static String dSorudetay;
    static String dModel;
    static String dFaaliyetKisim;
    static String dResimUri;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        String bla = data.getStringExtra("Bla");
        Firma =(String) data.getStringExtra("Firma");
        Sube = (String)data.getStringExtra("Sube");
        dDepartman =(String) data.getStringExtra("Departman");
        dFaaliyet =(String) data.getStringExtra("Faaliyet");
        dSoru =(String)data.getStringExtra("Soru");
        dSorudetay=(String)data.getStringExtra("Sorudetay");
        dModel=(String)data.getStringExtra("Model");
        dFaaliyetKisim=(String) data.getStringExtra("FaaliyetKisim");
        dResimUri=(String)data.getStringExtra("Resim");

        Log.d("MAinActivity Ornek",dResimUri);

        adapter.rixAnaliz.departmanList.get(adapter.SelectedDepartman.lvl1Section).
                faaliyetList.get(adapter.SelectedFaaliyet.lvl2Section).
                soruList.get(adapter.SelectedRisk.item).Detay=dSorudetay;

        adapter.rixAnaliz.departmanList.get(adapter.SelectedDepartman.lvl1Section).
                faaliyetList.get(adapter.SelectedFaaliyet.lvl2Section).
                soruList.get(adapter.SelectedRisk.item).Resim=dResimUri;

    }


	//String SelectedDepartman;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
//		final CoordinatorLayout coordinatorLayout;
//
//		coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);
		setContentView(R.layout.activity_main_calisma_ekrani);



        Bundle b = getIntent().getExtras();

        if (b != null) {
                            // that is the intent if from activity1 and contains additional parameters
                Firma =(String) b.get("Firma");
                Sube = (String) b.get("Sube");
                CompanyID =(Integer) b.get("FirmaID");
            }


        //final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		//final SwipeToRefreshLayout swipeToRefreshLayout = (SwipeToRefreshLayout) findViewById(R.id.swipe_refresh);
//
        //setSupportActionBar(toolbar);
//
		getSupportActionBar().setTitle(Firma+">"+Sube);

    	//getSupportActionBar().setSubtitle("Sample");

		final RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
		// bunlar fonksiyonlar testi için
		final Button btnFaaliyetEkle =(Button) findViewById(R.id.btnFaaliyetEkle);
		final Button btnBolumEkle =(Button) findViewById(R.id.btnBolumEkle);

		final TextView textBolum = (TextView) findViewById(R.id.textBolum);
		final EditText bolumgir=(EditText) findViewById(R.id.bolumgir);
        final Button bolumsil=(Button) findViewById(R.id.btnBolumSil);
		final Button faliyetsil=(Button) findViewById(R.id.btnFaaliyetSil);
		final Button x =(Button) findViewById(R.id.btnFaaliyetler);
		final Button btnKaydet =(Button) findViewById(R.id.btnKaydet);
        final Button btnDeneme =(Button) findViewById(R.id.deneme);



		recyclerView.setLayoutManager(new LinearLayoutManager(this));
		adapter = new Adapter(this);

		recyclerView.setAdapter(adapter);


		//adapter.setBucket(createBucket(0,0));

		adapter.setRixAnaliz(createRixAnaliz(4));

		textBolum.setText("Deneme");

		FloatingActionButton faEkle = (FloatingActionButton) findViewById(R.id.floFaaliyetEkle);
		btnKaydet.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Gson gson = new Gson();
//				adapter.rixAnaliz.sure=Calendar.getInstance().getTime().toString();
//
//				adapter.rixAnaliz.toplamRiskSayisi=adapter.getNumberOfLvl0Items();

				Random rn = new Random();
				int range = 5000000- 50000 + 1;
				int randomNum =  rn.nextInt(range) + 25000;

				String json = gson.toJson(adapter.rixAnaliz);
				Log.d("detay",adapter.rixAnaliz.firmaYetkilisi);
				MyJSONFile.saveData(getApplicationContext(),json,Firma+"_"+Integer.toString(randomNum)+"_rix.jsn");
				Log.d("json",json);

				String filename="serkan.json";

				File filelocation = new File(getApplicationContext().getFilesDir().getPath() , filename);
				Uri path = Uri.fromFile(filelocation);
				Intent emailIntent = new Intent(Intent.ACTION_SEND);
// set the type to 'email'
				emailIntent .setType("vnd.android.cursor.dir/email");
				String to[] = {"serkandemirhan@gmail.com"};
				emailIntent .putExtra(Intent.EXTRA_EMAIL, to);
// the attachment
				//emailIntent .putExtra(Intent.EXTRA_STREAM, path);
				emailIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse( "file://"+filelocation));
				Log.d("Emaila attach:",Uri.parse( "file://"+filelocation).toString());
// the mail subject
				emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject");
				emailIntent.putExtra(Intent.EXTRA_TEXT,json);
				startActivity(Intent.createChooser(emailIntent , "Send email..."));

			}
		});




		faEkle.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {


//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();

//				Toast.makeText(RixAnalizler.this, "ID:" +SelectedID+ " Company:" +SelectedCompany+
//						" sube:" +SelectedSube,Toast.LENGTH_LONG).show();


				Intent intent2 = new Intent(MainActivity.this, faaliyetEkle.class);
				intent2.putExtra("Firma",Firma);
				intent2.putExtra("Sube",Sube);
				intent2.putExtra("Departman","Departman1");

				startActivity(intent2);
			}
		});

        btnDeneme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String detayy=adapter.rixAnaliz.departmanList.get(adapter.SelectedDepartman.lvl1Section).
                        faaliyetList.get(adapter.SelectedFaaliyet.lvl2Section).
                        soruList.get(adapter.SelectedRisk.item).Detay;
                String resimy=adapter.rixAnaliz.departmanList.get(adapter.SelectedDepartman.lvl1Section).
                        faaliyetList.get(adapter.SelectedFaaliyet.lvl2Section).
                        soruList.get(adapter.SelectedRisk.item).Resim;

               Log.d("Detay.",detayy);
               Log.d("Resimn",resimy);



            }
        });



//		btnBolumEkle.setOnClickListener(new View.OnClickListener() {
//
//			String txtbolum= bolumgir.getText().toString();
//			@Override
//			public void onClick(View v) {
//				adapter.rixAnaliz.departmanList.add(createDepartman(11,txtbolum));
//				adapter.notifyDataSetChanged();
//
//				//adapter.rixAnaliz.departmanList.remove(1);
//			}
//		});

		x.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				System.out.println(jsontext);
				Departman d = new Gson().fromJson(jsontext, Departman.class);
				System.out.println(d);
				System.out.println(d.name);
				System.out.println(d.faaliyetList.get(0).name);
				Faaliyet f= d.faaliyetList.get(0);

				adapter.rixAnaliz.departmanList.get(adapter.SelectedDepartman.lvl1Section).faaliyetList.add(f);
				adapter.notifyDataSetChanged();

				//textBolum.setText(katalog.faaliyetList.get(1).name);
			}
		});

		btnFaaliyetEkle.setOnClickListener(new View.OnClickListener() {

			//String txtbolum= bolumgir.getText().toString();
			@Override
			public void onClick(View v) {

				try {
					String textparent = "se";
					//textparent = adapter.getCurrentItemIndex().toString();
					Departman departmanc = adapter.rixAnaliz.departmanList.get(adapter.SelectedDepartman.lvl1Section);
					Faaliyet faaliyetc = departmanc.faaliyetList.get(adapter.SelectedFaaliyet.lvl2Section);
					String txtchild = departmanc.name + "faaliyet adi"
							+ faaliyetc.id;
					//emptyView.setText();
					textBolum.setText(txtchild);
				}
				catch (Exception e){
					Toast.makeText(getApplicationContext(),"hata olustu", Toast.LENGTH_LONG).show();

				}

			}
		});

        bolumsil.setOnClickListener(new View.OnClickListener() {

            //String txtbolum= bolumgir.getText().toString();
            @Override
            public void onClick(View v) {

				try{
					Departman departmanc= adapter.rixAnaliz.departmanList.get(adapter.SelectedDepartman.lvl1Section);
					//String textparent="tum indisler";
					adapter.rixAnaliz.departmanList.remove(adapter.SelectedDepartman.lvl1Section);
					adapter.SelectedDepartman=null;
				}
				catch (Exception e ){
					Toast.makeText(getApplicationContext(),"silinemez", Toast.LENGTH_LONG).show();

				}
				adapter.closeAll();
                //adapter.isLvl1SectionOpened(0);
				recyclerView.setAdapter(adapter);
				recyclerView.refreshDrawableState();

            }
        });

		faliyetsil.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				try {

					adapter.rixAnaliz.departmanList.get(adapter.SelectedDepartman.lvl1Section).faaliyetList.remove(
					adapter.SelectedFaaliyet.lvl2Section);
					//adapter.rixAnaliz.

				}

				catch(Exception e){
					Toast.makeText(getApplicationContext(),"silinemez", Toast.LENGTH_LONG).show();
				}
				//adapter.closeAll();
				//adapter.isLvl1SectionOpened(0);
				recyclerView.setAdapter(adapter);
				recyclerView.refreshDrawableState();
			}
		});

//     swipeToRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//			@Override
//			public void onRefresh() {
//				//adapter.setRixAnaliz(createRixAnaliz(5));
//				String txtparent=recyclerView.getParent().toString();
//				String txtchild=recyclerView.getFocusedChild().toString();
//				//emptyView.setText();
//				//emptyView.setText(txtparent+txtchild);
//
//				swipeToRefreshLayout.setRefreshing(false);
//			}
//		});




	}

	private static Soru createSoru(Integer x, Integer y) {
		x = new Random().nextInt(10000) + 3;
		String durum = "durum";
		String name="risk "+ Integer.toString(y)+"faa"+ Integer.toString(x);
		Soru soru =new Soru(name,x,durum);
        soru.Detay="";
        soru.Resim="";
		return soru;
	}

	private static Faaliyet createFaaliyet(Integer t){
		t = new Random().nextInt(1000) + 2;
		String detay = "faliyet detay " + Integer.toString(t);
		String name="faliyet isim "+ Integer.toString(t);
		Faaliyet faaliyet =new Faaliyet(name,t,detay);

		for (int x=1;x<5;x++) {
			faaliyet.soruList.add(createSoru(x,t));
		}
		faaliyet.name=name + Integer.toString(faaliyet.toplam);
		faaliyet.detay=detay;
		faaliyet.id=t;
		return faaliyet;
	}

	private static Departman createDepartman(Integer t, String name){
		t = new Random().nextInt(10000) + 2;
		String detay = "dep detay" + Integer.toString(t);
		Departman departman = new Departman(name,t,detay);
		for (int x=1;x<5;x++) {
			if (x!=4) {
				departman.faaliyetList.add(createFaaliyet(t));
			}
			if (x==4){
				//departman.faaliyetList=null;

			}
		}
		departman.name=name;
		departman.detay=detay;
		departman.id=t;
		return departman;
	}

	private static RixAnaliz createRixAnaliz (Integer t){
		t = new Random().nextInt(t) +10;
		String operator= "operator" + Integer.toString(t);
		String name="Rix Analiz ismi "+ Integer.toString(t);
		RixAnaliz rixAnaliz = new RixAnaliz();
		rixAnaliz.name=name;
		rixAnaliz.operator=operator;
		rixAnaliz.id=t;
		for (int x=1;x<t;x++) {
			rixAnaliz.departmanList.add(createDepartman(x,name+"-Depart"+ Integer.toString(x)));
		}
		return rixAnaliz;
	}

	private static class Risk{
        String id;
        String tehlike;
        Boolean aep;
        String adi;
        String etkilenen;
        String planlanan;
        String onceki_frekans;
        String onceki_olasilik;
        String onceki_siddet;
        String frekans;
        String olasilik;
        String siddet;
        Integer termin;
        Integer sorumluID;
        String sorumlu;
    }

	private static class Soru {
		String name;
        String ServerNo;
        String Oncekicevap;
        String Detay;
        String Resim;
		Integer id;
		String cevap;
        ArrayList<Risk> riskList;
		public Soru(String name, Integer id, String durum){
			this.name=name;
            this.Detay="";
            this.Resim="";
			this.id=id;
			this.cevap=durum;
            riskList=new ArrayList<>();
		}
	}
	private static class Faaliyet {
		String name;
		Integer id;
		String detay;
		Integer toplam;
		ArrayList<Soru> soruList;

		public Faaliyet(String name, Integer id, String detay){
			this.id=id;
			this.name=name;
			this.detay=detay;
			soruList =new ArrayList<>();
			this.toplam=this.soruList.size();
		}
	}


	String jsontext =
			"{"+
		"\"name\":\"departman\","+
				"\"id\":5000,"+
				"\"detay\":\"asdfa\","+

	"faaliyetList:["+
	"{"+
  "\"name\": \"fa1 CNC\","+
  "\"id\": 1,"+
  "\"detay\":\"detaybilgi\","+
  "\"toplam\":34,"+
  "\"soruList\":["+
		"{"+
      "\"name\": \"Risk1 faaliyet1\","+
      "\"id\": 1,"+
      "\"cevap\":\"false\""+
		"},"+
		"{"+
      "\"name\": \"Risk2 faaliyet1\","+
      "\"id\": 2,"+
      "\"cevap\":\"true\""+
		"}]"+

	"},"+

	"{"+
  "\"name\": \"fa2 CNC\","+
  "\"id\": 2,"+
  "\"detay\":\"fa2 detaybilgi\","+
  "\"toplam\":34,"+
  "\"soruList\":["+
		"{"+
      "\"name\": \"fa1 Risk1 faaliyet1\","+
      "\"id\": 1,"+
      "\"cevap\":\"false\""+
		"},"+
		"{"+
      "\"name\": \"fa2 Risk2 faaliyet1\","+
      "\"id\": 2,"+
      "\"cevap\":\"true\""+
		"}"+
            "]}"+
"]"+
"}";





    public static class Departman {
		String name;
		Integer id;
		String detay;
		ArrayList<Faaliyet> faaliyetList;

		public Departman(String name, Integer id, String detay){
			this.id=id;
			this.detay=detay;
			this.name=name;
			faaliyetList =new ArrayList<>();
		}
	}

	private static class RixAnaliz {
		String name;
		Integer id;
		String operator="serkan";
		String tarih=Calendar.getInstance().getTime().toString();
		//sonra yazilacaklar
		String sure="15 dk";
		String firma=Firma;
		String sube=Sube;
		String lokasyon ="x12312,y123";
		Integer toplamRiskSayisi=12;
		String firmaYetkilisi="Aytekin Demirhan";;
		ArrayList<Departman> departmanList;




		public RixAnaliz(){
			this.id=id;
			this.operator=operator;
			this.name=name;
			this.tarih="10-10-2017 07:56";
			this.departmanList =new ArrayList<>();
			this.firmaYetkilisi="Aytekin Demirhan";
			this.lokasyon="Lat 012313, Long 234234234";
			this.firma=Firma;
			this.sube=Sube;
	}
	}




	private class Adapter extends SwiftAdapter<Adapter.ViewHolder> {

		//private Bucket bucket;
		public RixAnaliz rixAnaliz=new RixAnaliz();
		public ItemIndex SelectedDepartman;
		public ItemIndex SelectedFaaliyet;
		public ItemIndex SelectedRisk;




		private Context context;

		private LayoutInflater layoutInflater;

		public Adapter(Context context) {
			this.context = context;
  		    //RixAnaliz rixAnaliz =new RixAnaliz();
//			rixAnaliz.operator="serkan";
			SelectedDepartman=null;
			SelectedFaaliyet=null;
			layoutInflater = LayoutInflater.from(context);
		}
//
//		public void setBucket(Bucket bucket) {
//			closeAll();
//			this.bucket = bucket;
//			notifyDataSetChanged();
//		}

		public void setRixAnaliz(RixAnaliz rixAnalizc) {
			closeAll();
			this.rixAnaliz= rixAnalizc;
			notifyDataSetChanged();
		}

		public class ViewHolder extends RecyclerView.ViewHolder {

			public TextView title;
			public View icon1;
			public View icon2;
			public View icon3;
			public Button muaf;
			public Button kamera;
			public Button evet;
			public Button hayir;
			public ImageView expand;
			public TextView emptyview;


			public ViewHolder(View itemView) {
				super(itemView);
				title = (TextView) itemView.findViewById(R.id.title);

				icon1 = itemView.findViewById(R.id.icon1);
				icon2 = itemView.findViewById(R.id.icon2);
				icon3 = itemView.findViewById(R.id.icon3);
				expand = (ImageView) itemView.findViewById(R.id.expand);
				emptyview = (TextView) itemView.findViewById(R.id.emptyview);
			}
		}



		/**
		 * NO SECTION ROWS
		 **/

		@Override
		public int getNumberOfLvl0Items() {
			//return bucket.itemList.size();
			return 0;
		}

		public class Lvl0ItemViewHolder extends ViewHolder {

			public Lvl0ItemViewHolder(View itemView) {
				super(itemView);
			}
		}

		@Override
		public ViewHolder onCreateLvl0ItemViewHolder(ViewGroup parent) {

			return new Lvl0ItemViewHolder(layoutInflater.inflate(R.layout.custom_row_departman, parent, false));
		}

		@Override
		public void onBindLvl0Item(ViewHolder holder, ItemIndex index) {
			if (holder instanceof Lvl0ItemViewHolder) {

				Lvl0ItemViewHolder lvl0ItemViewHolder = (Lvl0ItemViewHolder) holder;
				String title = "0";
				lvl0ItemViewHolder.title.setText(title);
			}
		}

		@Override
		public int getNumberOfLvl1Sections() {
			//return bucket.bucketList.size();
			return this.rixAnaliz.departmanList.size();
		}

		public class Lv1SectionViewHolder extends ViewHolder {

			public Lv1SectionViewHolder(View itemView) {
				super(itemView);

				itemView.setBackgroundResource(R.color.departman);
			}

			public void setOpen(ItemIndex index) {
				if (isLvl1SectionOpened(index.lvl1Section)) {
					expand.setRotation(180);
					SelectedDepartman=index;

				} else {
					expand.setRotation(0);
				}
			}
		}

		@Override
		public ViewHolder onCreateLvl1SectionViewHolder(ViewGroup parent) {
			return new Lv1SectionViewHolder(layoutInflater.inflate(R.layout.custom_row_departman, parent, false));


		}

		@Override
		public void onBindLvl1Section(ViewHolder holder, final ItemIndex index) {
			if (holder instanceof Lv1SectionViewHolder) {

				final Lv1SectionViewHolder lv1SectionViewHolder = (Lv1SectionViewHolder) holder;

				String title = rixAnaliz
						.departmanList
						.get(index.lvl1Section)
						.name;
				Integer numberof =rixAnaliz
						.departmanList
						.get(index.lvl1Section).faaliyetList.size();

				lv1SectionViewHolder.title.setText(title +"-"+ numberof.toString()+"-" );

				//lvl0SectionViewHolder.icon1.setVisibility(View.VISIBLE);
				//lvl1SectionViewHolder.icon2.setVisibility(View.VISIBLE);
				//lvl2SectionViewHolder.expand.setVisibility(View.VISIBLE);

				lv1SectionViewHolder.icon1.setVisibility(View.VISIBLE);
				lv1SectionViewHolder.icon2.setVisibility(View.VISIBLE);
				lv1SectionViewHolder.icon3.setVisibility(View.VISIBLE);
				lv1SectionViewHolder.expand.setVisibility(View.VISIBLE);

				lv1SectionViewHolder.setOpen(index);


				lv1SectionViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View view) {
							openCloseLvl1Section(index);
							lv1SectionViewHolder.setOpen(index);
					}
				});
			}
		}

		/**
		 * SECTION ROWS
		 **/

		@Override
		public int getNumberOfLvl1ItemsForSection(int lvl1Section) {
//			return bucket.bucketList.get(lvl1Section)
//					.itemList.size();
			return 0;
		}

		public class Lvl1ItemViewHolder extends ViewHolder {

			public Lvl1ItemViewHolder(View itemView) {
				super(itemView);
			}
		}

		@Override
		public ViewHolder onCreateLvl1ItemViewHolder(ViewGroup parent) {
			return new Lvl1ItemViewHolder(layoutInflater.inflate(R.layout.custom_row_departman, parent, false));
		}

		@Override
		public void onBindLvl1Item(ViewHolder holder, ItemIndex index) {
			if (holder instanceof Lvl1ItemViewHolder) {

				Lvl1ItemViewHolder lvl1ItemViewHolder = (Lvl1ItemViewHolder) holder;

				String title="free";

				lvl1ItemViewHolder.icon1.setVisibility(View.VISIBLE);
				lvl1ItemViewHolder.icon2.setVisibility(View.VISIBLE);
				lvl1ItemViewHolder.expand.setVisibility(View.VISIBLE);

				lvl1ItemViewHolder.title.setText(title);
			}
		}

		/**
		 * SUBSECTIONS
		 **/

		@Override
		public int getNumberOfLvl2SectionsForSection(int section) {
			return rixAnaliz.departmanList.get(section)
					.faaliyetList.size();
		}

		public class Lvl2SectionViewHolder extends ViewHolder {

			public Lvl2SectionViewHolder(View itemView) {
				super(itemView);

				itemView.setBackgroundResource(R.color.faaliyet);
			}

			public void setOpen(ItemIndex index) {

				if (isLvl2SectionOpened(index.lvl1Section, index.lvl2Section)) {
					SelectedFaaliyet=index;
					expand.setRotation(180);

				} else {
					expand.setRotation(0);
				}
			}
		}

		@Override
		public ViewHolder onCreateLvl2SectionViewHolder(ViewGroup parent) {
			return new Lvl2SectionViewHolder(layoutInflater.inflate(R.layout.custom_faaliyet, parent, false));
		}

		@Override
		public void onBindLvl2Section(ViewHolder holder, final ItemIndex index) {
			if (holder instanceof Lvl2SectionViewHolder) {

				final Lvl2SectionViewHolder lvl2SectionViewHolder = (Lvl2SectionViewHolder) holder;

//				String title = bucket
//						.bucketList
//						.get(index.lvl1Section)
//						.bucketList
//						.get(index.lvl2Section)
//						.name;

				String title = rixAnaliz
						.departmanList
						.get(index.lvl1Section)
						.faaliyetList
						.get(index.lvl2Section)
						.name;
				Integer numberof=rixAnaliz
						.departmanList
						.get(index.lvl1Section)
						.faaliyetList
						.get(index.lvl2Section).soruList.size();

				Integer sayi = rixAnaliz
						.departmanList
						.get(index.lvl1Section)
						.faaliyetList
						.get(index.lvl2Section).toplam;




				lvl2SectionViewHolder.title.setText(title+"-"+numberof.toString()+"-"+sayi.toString());

				lvl2SectionViewHolder.icon1.setVisibility(View.VISIBLE);
				lvl2SectionViewHolder.icon2.setVisibility(View.VISIBLE);
				lvl2SectionViewHolder.expand.setVisibility(View.VISIBLE);

				lvl2SectionViewHolder.setOpen(index);

				lvl2SectionViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						openCloseLvl2Section(index);
						lvl2SectionViewHolder.setOpen(index);
					}
				});
			}
		}

		/**
		 * SUBSECTION ROWS
		 **/

		@Override
		public int getNumberOfLvl2ItemsForSection(int lvl1Section, int lvl2Section) {
			return rixAnaliz.departmanList.get(lvl1Section)
					.faaliyetList.get(lvl2Section)
					.soruList.size();
		}

		public class Lvl2ItemViewHolder extends ViewHolder {
			public Button muaf;
			public Button kamera;
			public Button evet;
			public Button hayir;

			public Lvl2ItemViewHolder(View itemView) {
				super(itemView);
				itemView.setBackgroundResource(R.color.notselectedrisk);
				evet =(Button)itemView.findViewById(R.id.btnEvet);
				hayir = (Button) itemView.findViewById(R.id.btnHayir);
				kamera =(Button) itemView.findViewById(R.id.btnKamera);
				muaf = (Button) itemView.findViewById(R.id.btnMuaf);
				emptyview.setText("serkan");

			}
		}

		@Override
		public ViewHolder onCreateLvl2ItemViewHolder(ViewGroup parent) {
			return new Lvl2ItemViewHolder(layoutInflater.inflate(R.layout.custom_risk, parent, false));
		}

		@Override
		public void onBindLvl2Item(ViewHolder holder, final ItemIndex index) {
			if (holder instanceof Lvl2ItemViewHolder) {

				final Lvl2ItemViewHolder lvl2ItemViewHolder = (Lvl2ItemViewHolder) holder;

				String title = rixAnaliz
						.departmanList
						.get(index.lvl1Section)
						.faaliyetList
						.get(index.lvl2Section)
						.soruList
						.get(index.item).
						name;

				String cevapText=	rixAnaliz.departmanList.get(index.lvl1Section).faaliyetList
						.get(index.lvl2Section).soruList.get(index.item).cevap;
                SelectedRisk=index;
				switch (cevapText) {
					case "Yes":
						lvl2ItemViewHolder.title.setBackgroundColor(Color.GREEN);
                        break;
					case "No":
						lvl2ItemViewHolder.title.setBackgroundColor(Color.RED);

						break;
					case "Muaf":
						lvl2ItemViewHolder.title.setBackgroundColor(Color.BLACK);

						break;

					default:
						lvl2ItemViewHolder.title.setBackgroundColor(Color.YELLOW);

						break;
				}


				lvl2ItemViewHolder.evet.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						rixAnaliz.departmanList.get(index.lvl1Section).faaliyetList
								.get(index.lvl2Section).soruList.get(index.item).cevap="Yes";
                                 notifyDataSetChanged();

					}

				});

                lvl2ItemViewHolder.kamera.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        SelectedRisk=index;

                        Intent intent2 = new Intent(MainActivity.this, detay.class);
                        intent2.putExtra("Firma",Firma);
                        intent2.putExtra("Sube",Sube);
                        intent2.putExtra("Departman","DEpartman");
                        intent2.putExtra("Faaliyet","Faaliyet");
                        intent2.putExtra("Soru","risk");
                        startActivityForResult(intent2,resultcode);
                                            }

                });

				lvl2ItemViewHolder.hayir.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						rixAnaliz.departmanList.get(index.lvl1Section).faaliyetList
								.get(index.lvl2Section).soruList.get(index.item).cevap="No";
                                notifyDataSetChanged();




					}
				});


				lvl2ItemViewHolder.muaf.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						//lvl2ItemViewHolder.itemView.findViewById(R.id.title).setTextColor

						rixAnaliz.departmanList.get(index.lvl1Section).faaliyetList
								.get(index.lvl2Section).soruList.get(index.item).cevap="Muaf";
                        notifyDataSetChanged();


//						rixAnaliz.departmanList.get(index.lvl1Section).faaliyetList
//								.get(index.lvl2Section).name+="Muaf 1";


					}
				});

				lvl2ItemViewHolder.title.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						String k="";
						k=	rixAnaliz.departmanList.get(index.lvl1Section).faaliyetList
								.get(index.lvl2Section).soruList.get(index.item).cevap;
						Toast.makeText(context,"Risk:"+k,Toast.LENGTH_SHORT).show();
					}
				});




                lvl2ItemViewHolder.icon1.setVisibility(View.VISIBLE);
				lvl2ItemViewHolder.icon2.setVisibility(View.VISIBLE);

				lvl2ItemViewHolder.icon3.setVisibility(View.VISIBLE);

				lvl2ItemViewHolder.title.setText(title);

				lvl2ItemViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View view) {

						//lvl2ItemViewHolder.itemView.setBackgroundResource(R.color.selectedrisk);
					}
				});




			}


		}

		/**
		 * SUBSUBSECTIONS
		 **/

//		@Override
//		public int getNumberOfLvl3SectionsForSection(int lvl1Section, int lvl2Section) {
//			return bucket.bucketList.get(lvl1Section)
//					.bucketList.get(lvl2Section)
//					.bucketList.size();
//		}

//		public class Lvl3SectionViewHolder extends ViewHolder {
//
//			public Lvl3SectionViewHolder(View itemView) {
//				super(itemView);
//
//				itemView.setBackgroundResource(R.color.subsubsection);
//			}
//
//			public void setOpen(ItemIndex index) {
//				if (isLvl3SectionOpened(index.lvl1Section, index.lvl2Section
//
//						, index.lvl3Section)) {
//					expand.setRotation(540);
//				} else {
//					expand.setRotation(0);
//				}
//			}
//		}
//
//		@Override
//		public ViewHolder onCreateLvl3SectionViewHolder(ViewGroup parent) {
//			return new Lvl3SectionViewHolder(layoutInflater.inflate(R.layout.custom_risk, parent, false));
//		}

	//	@Override
//		public void onBindLvl3Section(ViewHolder holder, final ItemIndex index) {
//			if (holder instanceof Lvl3SectionViewHolder) {
//
//				final Lvl3SectionViewHolder lvl3SectionViewHolder = (Lvl3SectionViewHolder) holder;
//
//				String title = bucket
//						.bucketList
//						.get(index.lvl1Section)
//						.bucketList
//						.get(index.lvl2Section)
//						.bucketList
//						.get(index.lvl3Section)
//						.name;
//
//				lvl3SectionViewHolder.title.setText(title);
//
//				lvl3SectionViewHolder.icon1.setVisibility(View.VISIBLE);
//				lvl3SectionViewHolder.icon2.setVisibility(View.VISIBLE);
//				lvl3SectionViewHolder.icon3.setVisibility(View.VISIBLE);
//				lvl3SectionViewHolder.expand.setVisibility(View.VISIBLE);
//
//				lvl3SectionViewHolder.setOpen(index);
//
//				lvl3SectionViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
//					@Override
//					public void onClick(View view) {
//						openCloseLvl3Section(index);
//						lvl3SectionViewHolder.setOpen(index);
//					}
//				});
//			}
//		}

		/**
		 * SUBSUBSECTION ROWS
		 **/

//		@Override
//		public int getNumberOfLvl3ItemsForSection(int lvl1Section, int lvl2Section, int lvl3Section) {
//			return bucket.bucketList.get(lvl1Section)
//					.bucketList.get(lvl2Section)
//					.bucketList.get(lvl3Section)
//					.itemList.size();
//		}

//		public class Lvl3ItemViewHolder extends ViewHolder {
//
//			public Lvl3ItemViewHolder(View itemView) {
//				super(itemView);
//			}
//		}
//
//		@Override
//		public ViewHolder onCreateLvl3ItemViewHolder(ViewGroup parent) {
//			return new Lvl3ItemViewHolder(layoutInflater.inflate(R.layout.custom_risk, parent, false));
//		}

//		@Override
//		public void onBindLvl3Item(ViewHolder holder, ItemIndex index) {
//			if (holder instanceof Lvl3ItemViewHolder) {
//
//				Lvl3ItemViewHolder lvl3ItemViewHolder = (Lvl3ItemViewHolder) holder;
//
//				String title = bucket
//						.bucketList
//						.get(index.lvl1Section)
//						.bucketList
//						.get(index.lvl2Section)
//						.bucketList
//						.get(index.lvl3Section)
//						.itemList
//						.get(index.item);
//
//				lvl3ItemViewHolder.icon1.setVisibility(View.VISIBLE);
//				lvl3ItemViewHolder.icon2.setVisibility(View.VISIBLE);
//				lvl3ItemViewHolder.icon3.setVisibility(View.VISIBLE);
//
//				lvl3ItemViewHolder.title.setText(title);
//			}
//		}
	}
}
