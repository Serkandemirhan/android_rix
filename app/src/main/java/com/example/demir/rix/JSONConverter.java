package com.example.demir.rix;

import com.google.gson.Gson;
import java.util.List;

public class JSONConverter {

    public MainActivity.Departman getconvert(String json){
        //Gson gson = new Gson();
        //JSONConverter obj = gson.fromJson(json, JSONConverter.class);

        MainActivity.Departman d = new Gson().fromJson(json, MainActivity.Departman.class);
        return d;
    }

}