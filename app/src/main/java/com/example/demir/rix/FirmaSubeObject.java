package com.example.demir.rix;

public class FirmaSubeObject {
    private int id;
    private String word;
    private String meaning;
    private Boolean status;
    public FirmaSubeObject(int id, String word, Boolean status) {
        this.id = id;
        this.word = word;
        this.status=status;

    }
    public FirmaSubeObject(int id, String word, String meaning, Boolean status) {
        this.id = id;
        this.word = word;
        this.meaning = meaning;
        this.status=status;
    }
    public int getId() {
        return id;
    }
    public String getWord() {
        return word;
    }
    public String getMeaning() {
        return meaning;
    }
    public Boolean getstatus() {
        return status;
    }
    public void setStatus(Boolean status) {
        this.status = status;
    }
}