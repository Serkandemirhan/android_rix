package com.example.demir.rix;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class AnalizAdapter extends RecyclerView.Adapter<AnalizAdapter.MyViewHolder> {

    private List<Analiz> analizList;
   // private final DialogInterface.OnClickListener mOnClickListener = new MyOnClickListener();

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView firma,sube,danisman,tarih;

        public MyViewHolder(View view) {
            super(view);
            firma = (TextView) view.findViewById(R.id.txtFirma);
            sube = (TextView) view.findViewById(R.id.txtSube);
            danisman = (TextView) view.findViewById(R.id.txtDanisman);
            tarih=(TextView) view.findViewById(R.id.txtTarih);
        }
    }

    public AnalizAdapter(List<Analiz> analizList) {
        this.analizList = analizList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_rix_analiz_context, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Analiz analiz = analizList.get(position);
        holder.firma.setText(analiz.getFirma());
        holder.sube.setText(analiz.getSube());
        holder.danisman.setText(analiz.getDanisman());
        holder.tarih.setText(analiz.getTarih());
    }



    @Override
    public int getItemCount() {
        return analizList.size();
    }
}

