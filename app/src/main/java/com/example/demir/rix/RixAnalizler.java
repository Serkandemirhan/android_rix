package com.example.demir.rix;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.Response;
import com.android.volley.error.VolleyError;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class RixAnalizler extends AppCompatActivity
    implements NavigationView.OnNavigationItemSelectedListener {
    public static final String PREFS_NAME = "KZEKA";
    private SharedPreferences sharedPreferences;

    private List<Analiz> tumRixAnalizList = new ArrayList<>();
    private RecyclerView mRecyclerView;
    private AnalizAdapter aAdapter;
    String SelectedCompany = "";
    String SelectedSube = "";
    Integer SelectedID = 0;
    public static int idSnf;//

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rix_analizler);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        initViews();//recyclerview initialize edildi.
        setData();//datayı set ettik.
        //setupAdapter();//datayı adapter'a gönderdik.

        aAdapter = new AnalizAdapter(tumRixAnalizList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(aAdapter);

        mRecyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(this, mRecyclerView, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        // do whatever

                        SelectedCompany = tumRixAnalizList.get(position).getFirma();
                        SelectedID = tumRixAnalizList.get(position).getId();
                        SelectedSube = tumRixAnalizList.get(position).getSube();
                        Toast.makeText(RixAnalizler.this, "ID:" + SelectedID + " Company:" + SelectedCompany +
                                " sube:" + SelectedSube, Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onLongItemClick(View view, int position) {
                        // do whatever
                        Toast.makeText(RixAnalizler.this, "uzunk" + position, Toast.LENGTH_LONG).show();
                    }
                })
        );


        //setContentView(R.layout.lstvw);//View sinifinin adi

        //sifre = new DBHandler(this);//  veritabani sinifinin nesnesi...(5. derste anlatilacak olan sinif)
        DBHandler db = new DBHandler(getApplicationContext());//Sqlite Nesnesi
        ArrayAdapter adaptor;


        //final ListView lvKayitlar=(ListView)findViewById(R.id.lv);

        adaptor = db.tumKayitlar(getApplicationContext());
        //lvKayitlar.setAdapter(adaptor);
        //lvKayitlar.setTextFilterEnabled(true);

        //Button Sync = (Button)findViewById(R.id.btnSync);
        Button Kayit = (Button) findViewById(R.id.btnKayit);
        Button Getir = (Button) findViewById(R.id.btnGetir);
        final TextView Adress = (TextView) findViewById(R.id.etAdres);
        Button Sil = (Button) findViewById(R.id.btnSil);

       /* Kayit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                DBHandler db = new DBHandler(getApplicationContext());
                int t;
                db.adresilEkle(Adress.getText().toString());

                lvKayitlar.setAdapter(db.tumKayitlar(getApplicationContext())); //REfresh için
                Toast.makeText(getApplicationContext(),"kaydedildi",Toast.LENGTH_LONG).show();

            }
        });*/
        Sil.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
//                DBHandler db = new DBHandler(getApplicationContext());
//                db.adresilSil(1);
//                Toast.makeText(getApplicationContext(),"silindi",Toast.LENGTH_LONG).show();
                List<String> serkan = MyJSONFile.files(getApplicationContext());

                for (int x = 1; x < serkan.size(); x++) {

                    Log.d("files" + Integer.toString(x), serkan.get(x));

                }
            }
        });
        Getir.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                DBHandler db = new DBHandler(getApplicationContext());
                String s = "dene";

                s = db.kitapDetay(2).toString();
                Toast.makeText(getApplicationContext(), "silindi" + s, Toast.LENGTH_LONG).show();
            }
        });


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);


        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();

                Toast.makeText(RixAnalizler.this, "ID:" + SelectedID + " Company:" + SelectedCompany +
                        " sube:" + SelectedSube, Toast.LENGTH_LONG).show();
                Intent intent2 = new Intent(RixAnalizler.this, RixAnalizEkle.class);
                intent2.putExtra("Firma", SelectedCompany);
                intent2.putExtra("Sube", SelectedSube);
                intent2.putExtra("FirmaID", SelectedID);
                startActivity(intent2);
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

    }


    private void initViews() {
        mRecyclerView = (RecyclerView) findViewById(R.id.rv_Rix_Analiz_List);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
    }
    /*
liste halinde bir data oluşturuldu. Eğer web servisten data çekiyorsanız buraya gerek yoktur. setupAdapter metodundaki mWordList yerine Web servisten dönen datayı parametre olarak gönderebilirsiniz.
*/


    private void setData() {
        tumRixAnalizList.add(new Analiz(1, 1, 1, "Atik Otomotiv", "Merkez", "8 Ekim", "serkan"));
        tumRixAnalizList.add(new Analiz(2, 2, 2, "Kurumsal ZEKA", "IStanbul OFis", "6 Ekim", "ahmet"));
        tumRixAnalizList.add(new Analiz(3, 3, 3, "XKurumsal ZEKA", "xIStanbul OFis", "3 Ekim", "nesligan"));
        tumRixAnalizList.add(new Analiz(4, 4, 4, "yKurumsal ZEKA", "yIStanbul OFis", "1 Ekim", "diger"));
        tumRixAnalizList.add(new Analiz(1, 1, 1, "Atik Otomotiv", "Merkez", "8 Ekim", "serkan"));
        tumRixAnalizList.add(new Analiz(2, 2, 2, "Kurumsal ZEKA", "IStanbul OFis", "6 Ekim", "ahmet"));
        tumRixAnalizList.add(new Analiz(3, 3, 3, "XKurumsal ZEKA", "xIStanbul OFis", "3 Ekim", "nesligan"));
        tumRixAnalizList.add(new Analiz(4, 4, 4, "yKurumsal ZEKA", "yIStanbul OFis", "1 Ekim", "diger"));
        tumRixAnalizList.add(new Analiz(1, 1, 1, "Atik Otomotiv", "Merkez", "8 Ekim", "serkan"));
        tumRixAnalizList.add(new Analiz(2, 2, 2, "Kurumsal ZEKA", "IStanbul OFis", "6 Ekim", "ahmet"));
        tumRixAnalizList.add(new Analiz(3, 3, 3, "XKurumsal ZEKA", "xIStanbul OFis", "3 Ekim", "nesligan"));
        tumRixAnalizList.add(new Analiz(4, 4, 4, "yKurumsal ZEKA", "yIStanbul OFis", "1 Ekim", "diger"));
        tumRixAnalizList.add(new Analiz(1, 1, 1, "Atik Otomotiv", "Merkez", "8 Ekim", "serkan"));
        tumRixAnalizList.add(new Analiz(2, 2, 2, "Kurumsal ZEKA", "IStanbul OFis", "6 Ekim", "ahmet"));
        tumRixAnalizList.add(new Analiz(3, 3, 3, "XKurumsal ZEKA", "xIStanbul OFis", "3 Ekim", "nesligan"));
        tumRixAnalizList.add(new Analiz(4, 4, 4, "yKurumsal ZEKA", "yIStanbul OFis", "1 Ekim", "diger"));
        tumRixAnalizList.add(new Analiz(1, 1, 1, "Atik Otomotiv", "Merkez", "8 Ekim", "serkan"));
        tumRixAnalizList.add(new Analiz(2, 2, 2, "Kurumsal ZEKA", "IStanbul OFis", "6 Ekim", "ahmet"));
        tumRixAnalizList.add(new Analiz(3, 3, 3, "XKurumsal ZEKA", "xIStanbul OFis", "3 Ekim", "nesligan"));
        tumRixAnalizList.add(new Analiz(4, 4, 4, "yKurumsal ZEKA", "yIStanbul OFis", "1 Ekim", "diger"));
        tumRixAnalizList.add(new Analiz(1, 1, 1, "Atik Otomotiv", "Merkez", "8 Ekim", "serkan"));
        tumRixAnalizList.add(new Analiz(2, 2, 2, "Kurumsal ZEKA", "IStanbul OFis", "6 Ekim", "ahmet"));
        tumRixAnalizList.add(new Analiz(3, 3, 3, "XKurumsal ZEKA", "xIStanbul OFis", "3 Ekim", "nesligan"));
        tumRixAnalizList.add(new Analiz(4, 4, 4, "yKurumsal ZEKA", "yIStanbul OFis", "1 Ekim", "diger"));
    }

    private void setupAdapter() {


    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.welcome, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private List<FirmaSubeObject> getListItemData() {
        List<FirmaSubeObject> listViewItems = new ArrayList<FirmaSubeObject>();
        listViewItems.add(new FirmaSubeObject(1, "Fa aliyet1", "Apple", false));
        listViewItems.add(new FirmaSubeObject(2, "Fark aliyet2", "Orange", false));
        listViewItems.add(new FirmaSubeObject(3, "Denem Faaliyet3", "Banana", true));
        listViewItems.add(new FirmaSubeObject(4, "XYC Faaliyet4", "Grape", false));
        listViewItems.add(new FirmaSubeObject(5, "cc Faaliyet5", "Mango", true));
        listViewItems.add(new FirmaSubeObject(6, "eeeFaaliyetPear", "Pear", false));
        listViewItems.add(new FirmaSubeObject(7, "reter FaaliyetPineapple", "Pineapple", false));
        listViewItems.add(new FirmaSubeObject(8, "bursiFaaliyetStrawberry", "Strawberry", false));
        listViewItems.add(new FirmaSubeObject(9, "dokuz FaaliyetCoconut", "Coconut", false));
        listViewItems.add(new FirmaSubeObject(10, "on FaaliyetAlmond", "Almond", true));
        return listViewItems;
    }

    public class VeriGuncelle extends AsyncTask<Void, Void, Boolean> {


        private String mSubdomain = "";
        public String mToken = "";
        public int xyz = 0;

        VeriGuncelle(){     }


        @Override
        protected Boolean doInBackground(Void... params) {

                mSubdomain = (sharedPreferences).getString("subdomain", "");
                mToken = (sharedPreferences).getString("token", "");
                /// get  vesiyon from android
                                if (sharedPreferences.contains("firmaversiyon")) {
                                    Integer uFirmalarVersiyon = (sharedPreferences).getInt("firmaversiyon", 0);
                                    Integer uServerFirmaVersion =5;
                                    if (uServerFirmaVersion>uFirmalarVersiyon){
                                        dosyalarigetir("firmalar");
                                    }
                                } else
                                {
                                       dosyalarigetir("firmalar");
                                }
                             /// versiyon yoksa dosyaları getir ve diske yaz
                                dosyalarigetir("firmalar");
                                if (sharedPreferences.contains("kullaniciversiyon")){
                                    Integer uKullanicilarVersiyon = (sharedPreferences).getInt("kullaniciversiyon", 0);
                                    Integer uServerKullanıcıVersion=5;
                                    // get server serviyon;
                                    if (uServerKullanıcıVersion>uKullanicilarVersiyon){
                                        dosyalarigetir("kullanicilar");
                                    }
                                } else
                                {
                                    dosyalarigetir("kullanicilar");
                                }
                                if (sharedPreferences.contains("katalogversiyon")) {
                                    Integer uKatalogVersiyon = (sharedPreferences).getInt("katalogversiyon", 0);
                                    Integer uServerKatalogVersion = 5;
                                    if (uServerKatalogVersion > uKatalogVersiyon) {
                                        dosyalarigetir("faaliyetler");
                                    }
                                }else
                                    {
                                        dosyalarigetir("faaliyetler");
                                    }
                        return true;
                }

                 public void dosyalarigetir(final String servisismi) {


            Response.Listener<String> dinleyici = new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d("xxResponse", response);
                    try {
                        String cevap = "";
                        JSONObject jsonObject = new JSONObject(response);
                        cevap = jsonObject.getJSONObject("response").get("status").toString();
                        //ret = response.substring(23, 26);
                        Log.d("ret", cevap);
                        if (cevap.equals("1")) {
                            MyJSONFile.saveData(getApplicationContext(), jsonObject.toString(), servisismi+".txt");
                            onPostExecute(true);
                        }
                    } catch (Exception e) {
                        Log.d("Dosyayi yazamadim", e.getMessage());
                        onPostExecute(false);
                        //offline login yapacagiz
                        //sharedPreferences = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
                    }
                    //  internet=false;
                    Log.d("Internet", "yok");
                    xyz = 3;
                    onPostExecute(false);
                    // System.out.print(e);
                }


            };

            Response.ErrorListener t = new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d("Errortttt", "hata:" + error.getMessage());
                }
            };

            InternetCheck.DataWebService.getInstance().tryGetir(getApplicationContext(), mToken, mSubdomain, "s_butun_"+servisismi, dinleyici, t);
        }
        @Override
        protected void onPostExecute ( final Boolean success) {
//            mAuthTask = null;
//            showProgress(false);
//
//            if (success) {
//                //Toast.makeText(LoginActivity.this,"sede",Toast.LENGTH_SHORT).show();
//                finish();
//            } else {
//
//
//                if (xyz == 2) {
//                    mPasswordView.setError(getString(R.string.error_incorrect_password));
//                    mPasswordView.requestFocus();
//                }
//                if (xyz == 3) {
//
//                    //Snackbar.make()Toast.makeText(getApplicationContext(),"Internet Yok",Toast.LENGTH_LONG).show();
//                    Snackbar snackbar = Snackbar.make(coordinatorLayout, "Internet bağlantınız yok", Snackbar.LENGTH_LONG);
//                    snackbar.show();
//                }
//                if (xyz == 4) {
//                    mDomainView.setError("Boyle bir domain yok!");
//                    mDomainView.requestFocus();
//                }
//                if (xyz == 5) {
//                    finish();
//                }
//            }


        }         //post execute

        @Override
        protected void onCancelled () {
//            mAuthTask = null;
//            showProgress(false);
        }

    } //veri guncelle

} //son

