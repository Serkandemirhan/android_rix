package com.example.demir.rix;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.app.LoaderManager.LoaderCallbacks;

import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;

import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.error.VolleyError;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static android.Manifest.permission.READ_CONTACTS;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity implements LoaderCallbacks<Cursor> {

    public static final String PREFS_NAME = "KZEKA";
    private SharedPreferences sharedPreferences;


    /**
     * Id to identity READ_CONTACTS permission request.
     */
    private static final int REQUEST_READ_CONTACTS = 0;

    /**
     * A dummy authentication store containing known user names and passwords.
     * TODO: remove after connecting to a real authentication system.
     */

    private UserLoginTask mAuthTask = null;

    // UI references.
    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private EditText mDomainView;
    private View mProgressView;
    private View mLoginFormView;
    CoordinatorLayout coordinatorLayout;

    ListView lv;
    ArrayAdapter adapter;
    ArrayList<HashMap<String, String>> kitap_liste;
    String kitap_adlari[];
    int kitap_idler[];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.burasi) ;

        // Set up the login form.
        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);
        populateAutoComplete();








        mPasswordView = (EditText) findViewById(R.id.password);
        mDomainView= (EditText) findViewById(R.id.domain) ;
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });


        try {
            sharedPreferences = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
            String uName = (sharedPreferences).getString("username", "");
            String uPasword = (sharedPreferences).getString("password", "");
            String uSubdomain= (sharedPreferences).getString("subdomain", "");
            String uToken = (sharedPreferences).getString("token", "");

            Log.d("Share",uName);

            mEmailView.setText(uName);
            mPasswordView.setText(uPasword);
            mDomainView.setText(uSubdomain);

        }   catch
                (Exception e){
        }


        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);
        Button btnAnaliz=(Button) findViewById(R.id.btnAnaliz);
        btnAnaliz.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent2 = new Intent(LoginActivity.this, RixAnalizler.class);
                startActivity(intent2);
            }
        });
        Button btnDetay=(Button) findViewById(R.id.btnDetay);
        btnDetay.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent2 = new Intent(LoginActivity.this, detay.class);
                startActivity(intent2);
            }
        });
        Button btnFaliyet=(Button) findViewById(R.id.btnFaliyet);
        btnFaliyet.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent2 = new Intent(LoginActivity.this, faaliyetEkle.class);
                startActivity(intent2);
            }
        });
        Button btnRixAnalizekle=(Button) findViewById(R.id.btnRixAnalizEkle);
        btnRixAnalizekle.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent2 = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(intent2);
            }
        });
        Button btnsync=(Button) findViewById(R.id.btnRixAnalizCEvapEkle);
        btnsync.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent21 = new Intent(LoginActivity.this, SyncActivity.class);
                startActivity(intent21);
            }
        });

        attemptLogin();





    }





    private void populateAutoComplete() {
        if (!mayRequestContacts()) {
            return;
        }

        getLoaderManager().initLoader(0, null, this);
    }

    private boolean mayRequestContacts() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (checkSelfPermission(READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        if (shouldShowRequestPermissionRationale(READ_CONTACTS)) {
            Snackbar.make(mEmailView, R.string.permission_rationale, Snackbar.LENGTH_INDEFINITE)
                    .setAction(android.R.string.ok, new View.OnClickListener() {
                        @Override
                        @TargetApi(Build.VERSION_CODES.M)
                        public void onClick(View v) {
                            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
                        }
                    });
        } else {
            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
        }
        return false;
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == REQUEST_READ_CONTACTS) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                populateAutoComplete();
            }
        }
    }


    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);
        mDomainView.setError(null);

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();
        String domain =mDomainView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for valid domain name
        if (TextUtils.isEmpty(domain)) {
            mDomainView.setError(getString(R.string.error_field_required));
            focusView = mDomainView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);
            mAuthTask = new UserLoginTask(email, password,domain);
            mAuthTask.execute((Void) null);
        }
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new CursorLoader(this,
                // Retrieve data rows for the device user's 'profile' contact.
                Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI,
                        ContactsContract.Contacts.Data.CONTENT_DIRECTORY), ProfileQuery.PROJECTION,

                // Select only email addresses.
                ContactsContract.Contacts.Data.MIMETYPE +
                        " = ?", new String[]{ContactsContract.CommonDataKinds.Email
                .CONTENT_ITEM_TYPE},

                // Show primary email addresses first. Note that there won't be
                // a primary email address if the user hasn't specified one.
                ContactsContract.Contacts.Data.IS_PRIMARY + " DESC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        List<String> emails = new ArrayList<>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            emails.add(cursor.getString(ProfileQuery.ADDRESS));
            cursor.moveToNext();
        }

        addEmailsToAutoComplete(emails);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }

    private void addEmailsToAutoComplete(List<String> emailAddressCollection) {
        //Create adapter to tell the AutoCompleteTextView what to show in its dropdown list.
        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(LoginActivity.this,
                        android.R.layout.simple_dropdown_item_1line, emailAddressCollection);

        mEmailView.setAdapter(adapter);
    }


    private interface ProfileQuery {
        String[] PROJECTION = {
                ContactsContract.CommonDataKinds.Email.ADDRESS,
                ContactsContract.CommonDataKinds.Email.IS_PRIMARY,
        };

        int ADDRESS = 0;
        int IS_PRIMARY = 1;
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {

        private final String mEmail;
        private final String mPassword;
        private final String mDomain;
        public final String mToken="";
        public int xyz=0;

        UserLoginTask(String email, String password,String domain) {
            mEmail = email;
            mPassword = password;
            mDomain=domain;

        }

        @Override
        protected Boolean doInBackground(Void... params) {

            Response.Listener<String> deneme = new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d("xxResponse", response);
                    try {
                        String cevap = "";
                        JSONObject jsonObject = new JSONObject(response);

                        cevap=jsonObject.getJSONObject("response").get("status").toString();
                        //ret = response.substring(23, 26);
                        Log.d("ret", cevap);
                        if (cevap.equals("rix")) {
                            String mtoken =jsonObject.getJSONObject("response").get("token").toString();
                            Log.d("Token",mtoken);
//                                internet=true;
                            Log.d("Internet var giris var", "var");

                            SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
                            SharedPreferences.Editor editor = settings.edit();
                            editor.putString("username", mEmail);
                            editor.putString("password", mPassword);
                            editor.putString("subdomain", mDomain);
                            editor.putString("token", mtoken);
                            editor.commit();

                            xyz = 1;

                            onPostExecute(true);

                        } else if (cevap.equals("error")) {
                            Log.d("Intervar_girisyanlis", "var");
                            xyz = 2;
                            onPostExecute(false);

                        }

                        else if (cevap.equals("no_client")) {
                            Log.d("Intervar_firma yok", "var");
                            xyz = 4;
                            onPostExecute(false);

                        }

                    } catch (Exception e) {
                        //offline login yapacagiz

                        //sharedPreferences = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
                        String uName = (sharedPreferences).getString("username", "");
                        String uPasword = (sharedPreferences).getString("password", "");
                        String uSubdomain= (sharedPreferences).getString("subdomain", "");
                        String uToken = (sharedPreferences).getString("token", "");

                        if (uName.isEmpty() && uPasword.isEmpty()  && uSubdomain.isEmpty()&& uToken.isEmpty() ){

                            xyz=5;

                        }
                        //  internet=false;
                        Log.d("Internet", "yok");
                        xyz = 3;
                        onPostExecute(false);
                        // System.out.print(e);
                    }



                }
            };


            Response.ErrorListener t = new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d("Errortttt", "hata:"+error.getMessage());
                    //sharedPreferences = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
                    String uName = (sharedPreferences).getString("username", "");
                    String uPasword = (sharedPreferences).getString("password", "");
                    String uSubdomain= (sharedPreferences).getString("subdomain", "");
                    String uToken = (sharedPreferences).getString("token", "");

                    if (!uName.isEmpty() && !uPasword.isEmpty()  && !uSubdomain.isEmpty()&& !uToken.isEmpty() ){

                        xyz=5;
                        Log.d("Internet", "yok");

                        onPostExecute(true);

                    }

                    xyz = 3;
                    //  internet=false;

                    // System.out.print(e);






                }
            };

            InternetCheck.WebService.getInstance().tryLogin(getApplicationContext(), mEmail, mPassword, mDomain, deneme, t);

            if (xyz == 2) {
                return true;
            } else {
                return false;
            }
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
            showProgress(false);

            if (success) {
                //Toast.makeText(LoginActivity.this,"sede",Toast.LENGTH_SHORT).show();
                finish();
            } else {


                if (xyz == 2) {
                    mPasswordView.setError(getString(R.string.error_incorrect_password));
                    mPasswordView.requestFocus();
                }
                if (xyz == 3) {

                    //Snackbar.make()Toast.makeText(getApplicationContext(),"Internet Yok",Toast.LENGTH_LONG).show();
                    Snackbar snackbar = Snackbar.make(coordinatorLayout, "Internet bağlantınız yok", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
                if (xyz == 4) {
                    mDomainView.setError("Boyle bir domain yok!");
                    mDomainView.requestFocus();
                }
                if (xyz == 5) {
                   finish();
                }
            }

        }

        protected void finish(){

                Intent intent = new Intent(LoginActivity.this, RixAnalizler.class);
                startActivity(intent);
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }
    }


}

