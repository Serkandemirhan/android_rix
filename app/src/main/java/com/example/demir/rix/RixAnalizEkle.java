package com.example.demir.rix;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

public class RixAnalizEkle extends AppCompatActivity {
//
    String Firma,Sube;
    Integer CompanyID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rix_analiz_ekle);

        Intent iin= getIntent();
        Bundle b = iin.getExtras();

        if(b!=null)
        {

            Firma =(String) b.get("Firma");
            Sube = (String) b.get("Sube");
            CompanyID =(Integer) b.get("FirmaID");

        }






        final Spinner dFirma = (Spinner)findViewById(R.id.spnFirma);
        final Spinner dSube = (Spinner)findViewById(R.id.spnSube);
        //final Spinner dSektor =(Spinner)findViewById(R.id.spnSektor);
        Button btnAnalizBasla=(Button)findViewById(R.id.btnAnaliz);


        final String[] firmas = new String[]{"Atik Otomotiv", "KurumsalZEKA", "ZYX"};
        String[] subes = new String[]{"Merkez", "Eryaman", "İstanbul Anadolu Yakası"};


        ArrayAdapter<String> adapterFirma = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, firmas);
        ArrayAdapter<String> adapterSube = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, subes);


        dFirma.setAdapter(adapterFirma);
        dSube.setAdapter(adapterSube);

        dFirma.setSelection(CompanyID);
        dSube.setSelection(CompanyID);

        btnAnalizBasla.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent2 = new Intent(RixAnalizEkle.this, MainActivity.class);
                intent2.putExtra("Firma",dFirma.getSelectedItem().toString());
                intent2.putExtra("Sube",dSube.getSelectedItem().toString());
                intent2.putExtra("FirmaID",CompanyID);

                startActivity(intent2);
            }
        });



    }

}
