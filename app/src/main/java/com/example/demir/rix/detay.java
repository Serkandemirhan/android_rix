package com.example.demir.rix;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

public class detay extends AppCompatActivity {

    static String Firma;
    static String Sube,Departman,Faaliyet,Soru;
    static String Sorudetay,Model,FaaliyetKisim,Resim="";
    Integer CompanyID;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detay_ekle);
        Intent iin= getIntent();
        Bundle b = iin.getExtras();

        if(b!=null)
        {

            Firma =(String) b.get("Firma");
            Sube = (String) b.get("Sube");
            //CompanyID =(Integer) b.get("FirmaID");
            Departman =(String) b.get("Departman");
            Faaliyet =(String) b.get("Faaliyet");
            Soru =(String) b.get("Soru");



        }

        final EditText txtAciklama=(EditText) findViewById(R.id.txtAciklama);
        final EditText txtModel=(EditText) findViewById(R.id.txtModelMarka);
        final EditText txtKisim=(EditText) findViewById(R.id.txtKisim);



        ImageButton btnResim = (ImageButton) findViewById(R.id.imageButton);
        btnResim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent kamera_intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(kamera_intent, 44);
            }
        });

        Button btnDetayEkle=(Button) findViewById(R.id.btnDetayEkle);

        btnDetayEkle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent2 = new Intent(detay.this, MainActivity.class);
                intent2.putExtra("Firma",Firma);
                intent2.putExtra("Sube",Sube);
                intent2.putExtra("Departman",Departman);
                intent2.putExtra("Faaliyet",Faaliyet);
                intent2.putExtra("Soru",Soru);
                intent2.putExtra("Sorudetay",txtAciklama.getText());
                intent2.putExtra("Model",txtModel.getText());
                intent2.putExtra("FaaliyetKisim",txtKisim.getText());
                intent2.putExtra("Resim", Resim);
                setResult(1000,intent2);
                finish();

            }
        });


    }

    @Override
    protected void onActivityResult (int requestCode,int resultCode,Intent data){
        if (requestCode==44){

            Bitmap Image=(Bitmap)data.getExtras().get("data");
            ImageView resim=(ImageView) findViewById(R.id.imageView);
            resim.setImageBitmap(Image);
            Resim = storeImage(Image).toString();
        }

    }
    private File getOutputMediaFile(){
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory()
                + "/Android/data/"
                + getApplicationContext().getPackageName()
                + "/Files");

        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdirs()){
                return null;
            }
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmm").format(new Date());
        File mediaFile;

        Random rn = new Random();
        int range = 50000 + 1;
        int randomNum =  rn.nextInt(range) + 10000;
        String mImageName="rix_"+ Integer.toString(randomNum)+timeStamp +".jpg";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }

    private File storeImage(Bitmap image) {
        String TAG="MediaFile Save";
        File pictureFile = getOutputMediaFile();
        if (pictureFile == null) {
            Log.d(TAG,
                    "Error creating media file, check storage permissions: ");// e.getMessage());
            return null;
        }
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            image.compress(Bitmap.CompressFormat.JPEG, 90, fos);
            fos.close();
            Log.d(TAG, "Picture file name: " + pictureFile);
            return pictureFile;
        } catch (FileNotFoundException e) {
            Log.d(TAG, "File not found: " + e.getMessage());
        } catch (IOException e) {
            Log.d(TAG, "Error accessing file: " + e.getMessage());
        }
        return null;
    }


}