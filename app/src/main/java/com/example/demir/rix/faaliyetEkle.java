package com.example.demir.rix;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class faaliyetEkle extends AppCompatActivity {



    String Firma,Sube,Departmanx;
    Integer CompanyID;
    //String SelectedDepartman;
    private static final String TAG = "faaliyetEkle";
    // List of all dictionary words
    private List<FirmaSubeObject> dictionaryWords;
    private List<FirmaSubeObject> filteredList;
    // RecycleView adapter object
    private SimpleItemRecyclerViewAdapter mAdapter;
    // Search edit box
    private EditText searchBox;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faliyet_ekle);
        dictionaryWords = getListItemData();
        filteredList = new ArrayList<FirmaSubeObject>();
        filteredList.addAll(dictionaryWords);
        searchBox = (EditText)findViewById(R.id.search_box);
        RecyclerView recyclerView = (RecyclerView)findViewById(R.id.item_list);
        recyclerView.addItemDecoration(new SimpleDividerItemDecoration(this));
        assert recyclerView != null;
        mAdapter = new SimpleItemRecyclerViewAdapter(this,filteredList);
        recyclerView.setAdapter(mAdapter);


        Intent iin= getIntent();
        Bundle b = iin.getExtras();

        if(b!=null)
        {

            Firma =(String) b.get("Firma");
            Sube = (String) b.get("Sube");
            Departmanx =(String) b.get("Departman");


        }

        //final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //final SwipeToRefreshLayout swipeToRefreshLayout = (SwipeToRefreshLayout) findViewById(R.id.swipe_refresh);
//
        //setSupportActionBar(toolbar);
//
        getSupportActionBar().setTitle(Firma+">"+Sube +">"+Departmanx);
        // search suggestions using the edittext widget
        searchBox.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mAdapter.getFilter().filter(s.toString());
            }
            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    // create a custom RecycleViewAdapter class
    public class SimpleItemRecyclerViewAdapter extends RecyclerView.Adapter<SimpleItemRecyclerViewAdapter.ViewHolder> implements Filterable {



        private List<FirmaSubeObject> mValues;
        private LayoutInflater inflater;
        private CustomFilter mFilter;
        private Context ctx;
        public SimpleItemRecyclerViewAdapter(Context ctx,List<FirmaSubeObject> items) {
            inflater=LayoutInflater.from(ctx);
            mValues = items;
            mFilter = new CustomFilter(SimpleItemRecyclerViewAdapter.this);
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_faliyet_content, parent, false);
            return new ViewHolder(view);
        }
        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            holder.mItem = mValues.get(position);
            holder.mIdView.setText(String.valueOf(mValues.get(position).getId()));
            holder.mContentView.setText(mValues.get(position).getWord());
            holder.mCheckBox.setChecked(mValues.get(position).getstatus());

                        // holder.checkBox.setTag(R.integer.btnplusview, convertView);
            final int pos = position;
            holder.mCheckBox.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    //Toast.makeText(ctx, " Just for texting  clicked!", Toast.LENGTH_SHORT).show();
                    if(filteredList.get(position).getstatus()) {
                        filteredList.get(position).setStatus(false);
                    }else {
                        filteredList.get(position).setStatus(true);



                    };

                }
            });



        }


        @Override
        public int getItemCount() {
            return mValues.size();
        }
        @Override
        public Filter getFilter() {
            return mFilter;
        }



        public class ViewHolder extends RecyclerView.ViewHolder {
            public final View mView;
            public final TextView mIdView;
            public final TextView mContentView;
            public final CheckBox mCheckBox;
            public FirmaSubeObject mItem;



            public ViewHolder(View view) {
                super(view);
                mView = view;
                mIdView = (TextView) view.findViewById(R.id.id);
                mContentView = (TextView) view.findViewById(R.id.content);
                mCheckBox=(CheckBox) view.findViewById(R.id.checkbox);
            }
            @Override
            public String toString() {
                return super.toString() + " '" + mContentView.getText() + "'";
            }

/*
            @Override
            public void onClick(View v) {
                Toast.makeText(ctx, " Just for texting  clicked!", Toast.LENGTH_SHORT).show();

            }*/
        }
        public class CustomFilter extends Filter {
            private SimpleItemRecyclerViewAdapter mAdapter;
            private CustomFilter(SimpleItemRecyclerViewAdapter mAdapter) {
                super();
                this.mAdapter = mAdapter;
            }
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                filteredList.clear();
                final FilterResults results = new FilterResults();
                if (constraint.length() == 0) {
                    filteredList.addAll(dictionaryWords);
                } else {
                    final String filterPattern = constraint.toString().toLowerCase().trim();
                    for (final FirmaSubeObject mWords : dictionaryWords) {
                        if (mWords.getWord().toLowerCase().startsWith(filterPattern)) {
                            filteredList.add(mWords);
                        }
                        if (mWords.getWord().toLowerCase().contains(filterPattern)) {
                            filteredList.add(mWords);
                        }
                    }
                }
                System.out.println("Count Number " + filteredList.size());
                results.values = filteredList;
                results.count = filteredList.size();
                return results;
            }
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                System.out.println("Count Number 2 " + ((List<FirmaSubeObject>) results.values).size());
                this.mAdapter.notifyDataSetChanged();
            }
        }
    }
    private List<FirmaSubeObject> getListItemData(){
        List<FirmaSubeObject> listViewItems = new ArrayList<FirmaSubeObject>();

        /*
        listViewItems.add(new FirmaSubeObject(1, "Fa aliyet1", "Apple",false));
        listViewItems.add(new FirmaSubeObject(2, "Fark aliyet2", "Orange",false));
        listViewItems.add(new FirmaSubeObject(3, "Denem Faaliyet3", "Banana",true));
        listViewItems.add(new FirmaSubeObject(4, "XYC Faaliyet4", "Grape",false));
        listViewItems.add(new FirmaSubeObject(5, "cc Faaliyet5", "Mango",true));
        listViewItems.add(new FirmaSubeObject(6, "eeeFaaliyetPear", "Pear",false));
        listViewItems.add(new FirmaSubeObject(7, "reter FaaliyetPineapple", "Pineapple",false));
        listViewItems.add(new FirmaSubeObject(8, "bursiFaaliyetStrawberry", "Strawberry",false));
        listViewItems.add(new FirmaSubeObject(9, "dokuz FaaliyetCoconut", "Coconut",false));
        listViewItems.add(new FirmaSubeObject(10, "on FaaliyetAlmond", "Almond",true));
        return listViewItems;

        */

        listViewItems.add(new FirmaSubeObject(1,"Havalandırma","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Daire Testere Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"CNC Dik İşleme(Freze) Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Freze Tezgahı(Üniversal) Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Sütunlu Matkap İle Çalışma","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Plastik Enjeksiyon Makinesi Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Hava Kompresörü Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Plastik Kırma Makinesi Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Asansör Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Dalma Erezyon Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Çalışma Alanında Bulunan Vinç/Caraskal","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"KKD Kullanılmaması","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Acil Durum Meydana Gelmesi","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Elektriksel Temas","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Bakım Onarım İşleri","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Gürültü İşler Yapılması","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Postür Bozuklukları","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Sağlık Raporu Düzenleme","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Sağlık Taraması Yapımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Ark Kaynağı Yapma","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Güvensiz Hareketlerde Bulunma","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Uygun Olmayan İstifleme","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Zeminin Kayganlaşmasını Sağlayacak İşlemler","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Ofis Çalışmaları","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Tozlu İşlerin Yapılması","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Özel İşlerin Yapılması","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Yangın Meydana Gelmesi","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Eğitimlerin Yetersiz Olması","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"İşletmeye Giriş Çıkışlar","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Spiral Taşlama Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Taş Makinesi Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Ağır Malzemelerin El İle Taşınması","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Uygun Olmayan Termal Şartlar","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Uygun İş Elbisesi Olmaması","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Yetersiz Aydınlatma Düzeyi","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"El Aletleri","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Elektrikli El Aletleri","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Elektrikli El Aletlerinin Fişe Takılı Bırakılması","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Forklift Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Parlayıcı Patlayıcı Madde Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"LPG Tankı Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Lift Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Plastik Enjeksiyon Makinesine Kalıp Bağlanması","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Plastik Enjeksiyon Kalıplarının İstiflenmesi","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Yürüyüş Yolları ve Geçitler","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"CNC Torna Makinesi Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Vinç Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Azdırma Tezgahı Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Dik Planya Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Elektrik Direnç(Punta) Kaynağı Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Fellow(Dişli Açma) Tezgahı Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Gaz Altı Kaynağı Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Gaz Ergitme Kaynağı Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Kaynak İşlemi","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Yatay Şerit Testere Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Kayganlaşan Zemin","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Hijyenik Olmayan Ortam","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Kaplama İşlemi(Galvaniz, Krom, Nikel)","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Eleme Makinesi Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Mikser Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Hidrolik Pres Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Eksantrik(Mekanik) Pres Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Paketleme Makinesi Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Fırın Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Raflara Malzeme Koyma","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Baskı(Ofset) Makinesi Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Tel Çekme Makinesi Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Dilimleme Makinesi Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Siloda Çalışma","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Gemici Merdiveni","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Taşlama Tezgahı Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Takoz/Gönye Kesme Makinesinde Çalışma","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Torna Tezgahında(Üniversal) Çalışma","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Alttan Çıkma Çıta Kesim Makinesi Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"CNC Profil Kesim Makinesi Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"CNC Profil İşleme Makinesi Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Profil Kertme Makinesi Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Profil Köşe Pres Makinesi Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"CNC Kompozit İşleme Makinesi Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Fümigasyon(İlaçlama) İşlemi","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Raylı Kapıdan Geçiş Yapılması","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"AGROMEL Makinesi Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Oksi Asetilen Kaynağı Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Tig Kaynağı Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Bant(Palet) Zımpara Makinesi Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Baş Kesme Makinesi Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Borwerk Tezgahı Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Boya Tabancası Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Broş(Kama Açma) Tezgahı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Caraskal Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Çoklu Delik Delme Makinesi Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Giyotin Makas Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Hammadde Karıştırma Kazanı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Laminat Kesme Makinesi","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Masifleme Makinesi Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Oksijen Kesim Takımı Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Planya Tezgahı Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Postforming(Laminat Kıvırma) Makinesi","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Streçleme Makinesi","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Metal Talaş Atıkları","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Şerit Testere Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Transpalet Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Manlift Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Gırgır Vinç Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Elektrik","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Jeneratör Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"CNC Lazer Kesim Makinesi Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Boya Kabini Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Boya Fırını Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Abkant Pres(Büküm) Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Vibrasyon Makinesi Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Kurutma(Nem Alma) Makinesi-Fırını Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Yıkama(Temizleme) Kabini","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Vernik Atma İşlemi","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Diş(Klavuz) Çekme Makinesi Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Çift Kafa(Köşe) Kaynak Makinesi Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Köşe Temizleme Makinesi Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Kapı Kol Kilit Yeri Açma Makinesi Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Su Tahliye Kanalı Açma Makinesi Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Orta Kayıt Alıştırma Makinesi Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Boru Bükme Makinesi Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Honlama Tezgahı Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Hava Tabancası Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Yüzey Zımpara Makinesi","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Genişletilmiş Sac Makinesi Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Kumlama Tabancası Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Otomatik Kumlama Kabini Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Çelik İskele Kalas Çekme Makinası Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Alüminyum Freze Makinesi Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Delikli Sac Kesme Makinesi","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Maket Bıcağı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Rotasyon Makinesi Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"CNC Cam Kesme Makinesi","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Extruder(Ekstruder) Makinesi Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Rodaj Makinesi Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Sıcak Baskı ve Presleme Makinesi Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Temper Fırını Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Cam Delme(Delik Delme) Makinesi Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Cam Yıkama Makinesi Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"CNC Router Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Kuyu Tipi Tav Fırını","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Çan Tipi Tav Fırını","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Plastisol Makinesi","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Fiber Makinesi","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Ambalajlama Makinesi","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Robotik Kaynak Makinesi","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Silindirik Büküm Tezgahı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"Wpc Makinesi Kullanımı","Apple",false));
        listViewItems.add(new FirmaSubeObject(1,"CNC Modelleme Tezgahı","Apple",false));
        return listViewItems;
    }
}